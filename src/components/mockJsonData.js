export default function genData(x) {
  const data = []
  for (let i = 0; i < x; i++) {
    data.push({
      fullName: "สมศรี เจริญพร_" + randomid(2),
      propertyType: randomsPropertyType(),
      size: "1 ไร่ / 4 งาน / 20 ตารางวา",
      where: "จตุจักร /  กรุงเทพมหานคร",
      obligation: randomsObligation(),
      requestedValue: "-",
      responsiblePerson: randomsResponsiblePerson(),
      registrationChannel: "-",
      dateOfArrived: "20/11/2021",
      status: randomsConditions(3),
      information: randomsConditions(2),
      considerStatus: randomsConditions(2),
      responsiblePersonStatus: randomsConditions(3),
      id: randomid(20)
    })
  }
  return data
}

function randomsPropertyType() {
  const val = Math.floor(Math.random() * 2);
  if (val === 0) {
    return "บ้านเดี่ยว"
  } else if (val === 1) {
    return "ที่ดิน"
  } else if (val === 2) {
    return "คอนโดมิเนี่ยม"
  }
}

function randomsObligation() {
  const val = Math.floor(Math.random() * 2);
  if (val === 0) {
    return "สมศรี บุญมั่งมี"
  } else if (val === 1) {
    return "-"
  }
}

function randomsResponsiblePerson() {
  const val = Math.floor(Math.random() * 1);
  if (val === 0) {
    return "ปลอดภาระ"
  } else if (val === 1) {
    return "ติดภาระกับบุคคล (เช่นการขายฝาอะไรก็ไม่รู้ที่ข้อความยาวๆจนต้องใส่จุดไข่ปลาแทน)"
  } else if (val === 2) {
    return "ติดภาระกับธนาคาร"
  }
}

function randomsConditions(max) {
  const val = Math.floor(Math.random() * max);
  if (val === 0) {
    return "one"
  } else if (val === 1) {
    return "two"
  } else if (val === 2) {
    return "three"
  }
}

function randomid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() *
      charactersLength));
  }
  return result;
}
